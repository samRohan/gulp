var gulp = require('gulp');
var spa = require('gulp-spa');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rev = require('gulp-rev');
var del = require('del');
var less = require('gulp-less');
var cssmin = require('gulp-cssmin');
var autoprefixer = require('gulp-autoprefixer');
var uncss = require('gulp-uncss');
var plumber = require('gulp-plumber');
var uncss = require('gulp-uncss');
var livereload = require('gulp-livereload');
var imagemin = require('gulp-imagemin');
var pngcrush = require('imagemin-pngcrush');

var devDir = 'app/dev/',
	distDir = 'app/dist/';
var $paths = {
	dev: {
		css: devDir+'/css/',
		js: devDir+'/js/',
		images: devDir+'/images/'	
	},
	dist: {
		css: distDir+'/css/',
		js: distDir+'/js/',
		images: distDir+'/images/'	
	}
}
var $files = {
	dev: {
		less: [devDir+'less/*.less',devDir+'less/*/*.less'],
		html: [devDir+'index.html', devDir+'bible.html'],
		css: devDir+'css/*.css',
		js: devDir+'js/*.js'	
	},
	dist: {
		less: distDir+'less/*.less',
		html: distDir+'*.html',
		css: distDir+'css/*.css',
		js: distDir+'js/*.js',
	}
}
gulp.task('less', function() {
	gulp.src($files.dev.less[0])
		.pipe(plumber())
		.pipe(less())
		.pipe(gulp.dest($paths.dev.css));
});
gulp.task('imgmin', function() {
    return gulp.src($paths.dev.images+"*")
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngcrush()]
        }))
        .pipe(gulp.dest($paths.dist.images));
});
gulp.task('watch', function() {
	livereload.listen();
	gulp.watch($files.dev.less, ['less']);
	gulp.watch([$files.dev.html, $files.dev.css, $files.dev.js])
		.on('change', livereload.changed);
});
gulp.task('prepare', function () {
	del([$files.dist.js, $files.dist.css], function(){});
    return gulp.src($files.dev.html)
        .pipe(spa.html({
            assetsDir: devDir,
            pipelines: {
                js: function (files) {
                    return files
						.pipe(concat('js/app.js'))
                        .pipe(uglify())
                        .pipe(rev());
                },
                css: function (files) {
                	return files
                		.pipe(concat('css/stylesheet.css'))
                		.pipe(uncss({ html: $files.dev.html }))
                		.pipe(cssmin())
                		.pipe(rev());
                }
            }
        }))
    .pipe(gulp.dest(distDir));
});

gulp.task('build',['prepare','imgmin']);